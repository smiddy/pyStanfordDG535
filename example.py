import stanfordDG535 as sf

with sf.stanfordDG535Control('COM5') as trigBox:
    trigBox.setTrigMode(0)
    trigBox.setTrigRate(0, 20)
    print('Status of trigBox:', trigBox.isBusy())
