import serial
import logging
from decimal import Decimal

# Version
__version__ = '0.1.1'

# create logger
logger = logging.getLogger(__name__)


class stanfordDG535Error(Exception):
    """
    Stanford DG535 expection handle
    """

    def __init__(self, err):
        if (not err) or (b'\r\n' == err):
            self.errStr = 'Error byte response empty'
            return
        try:
            errCode = int(err[:-2].decode('ASCII'))
        except ValueError:
            self.errStr = 'Unkown error code: ' + err[:-2].decode('ASCII')
            return
        if 1 == errCode:
            self.errStr = 'Unrecognized command'
        elif 2 == errCode:
            self.errStr = 'Wrong number of parameters'
        elif 4 == errCode:
            self.errStr = 'Value outside allowed range.'
        elif 8 == errCode:
            self.errStr = 'Wrong mode for the command.'
        elif 16 == errCode:
            self.errStr = 'Delay linkage error.'
        elif 32 == errCode:
            self.errStr = 'Delay range error.'
        elif 64 == errCode:
            self.errStr = 'Recalled data was corrupt.'
        else:
            self.errStr = 'Unkown error code: ' + str(errCode)

    def __str__(self):
        return self.errStr


class stanfordDG535Control(object):
    '''API for the Stanford Research Systems DG535 Digital Pulse Generator

    This class provides an API for controll of the Stanford Research Systems
    DG535 Digital Pulse Generator.
    The latest values of the device are stored in the class object to ease the
    control.

    :param comPort: address of serial port
    :type comPort: string

    Example::
        import stanfordDG535 as sf

        with sf.stanfordDG535Control('COM12') as trigBox:
            trigBox.setTrigMode(0)
            trigBox.setTrigRate(0, 20)
            print('Status of trigBox:'. trigBox.isBusy())

    '''

    def __init__(self, comPort):
        self.__version__ = __version__
        self.comLink = serial.Serial(comPort, baudrate=9600,
                                     stopbits=serial.STOPBITS_ONE,
                                     bytesize=serial.EIGHTBITS,
                                     timeout=0.5,
                                     parity=serial.PARITY_NONE)
        self.logger = logging.getLogger(__name__)
        # Reset buffers
        self.comLink.reset_input_buffer()
        self.comLink.reset_output_buffer()
        # Configure the prologix box
        self.sendRcv('++addr 15', False)
        self.sendRcv('++mode 1', False)
        self.sendRcv('++auto 1', False)
        self.sendRcv('++eos 3', False)
        self.sendRcv('++eoi 1', False)
        self.sendRcv('++eot_enable 0', False)
        # Find address of triggerbox
        if not self.sendRcv('ES'):
            self.logger.warning('stanford not on default port')
            connected = False
            ii = 0
            while not connected:
                if ii == 31:
                    self.logger.error('Cannot connect to Stanford DG535.')
                    raise RuntimeError('Cannot connect to Stanford DG535.')
                self.sendRcv('++addr ' + str(ii), False)
                if not self.sendRcv('ES'):
                    ii += 1
                else:
                    connected = True
                    self.logger.debug('stanford initialized.')
        # Get the settings
        self._getSettings()

    def sendRcv(self, command, needAnswer=True):
        '''Takes a command and send it

        The string is converted to a compatible byte and send
        to the delay generator. The answer is analysed and in
        case a stanfordDG535Error is raised.
        The boolean needAnswer is important for GPIB communication. When set to
        False, the prologix controller is set to auto 0 to avoid error of
        trigger box.

        :param command: Command string
        :type: string
        :param needAnswer: set False if no answer expected. Default True.
        :type: boolean
        :returns: answer
        :rtype: string
        '''
        if command.startswith('++'):
            sendByte = bytearray(command, 'ASCII') + b'\r\n'
        else:
            sendByte = bytearray(command, 'utf-16le') + b'\r\n'
        if not self.comLink.isOpen():
            self.comLink.open()
        self.comLink.write(sendByte)
        if needAnswer:
            answer = self.comLink.readline()
        if not command.startswith('++'):
            self._errCheck()
        if needAnswer:
            return answer[:-2].decode('ASCII')
        else:
            return

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.setTrigMode(2)
        self.comLink.close()

    def _errCheck(self):
        if not self.comLink.isOpen():
            self.comLink.open()
        self.comLink.write(bytes('ES', 'utf-16le') + b'\n')
        self.comLink.write(b'++read eoi\n')
        err = self.comLink.readline()
        if err != b'0\r\n':
            raise stanfordDG535Error(err)
        return

    def _getSettings(self):
        '''Gets ALL device settings. May take a few seconds...
        '''
        self.getTrigMode()
        self.getExtTrigLevel()
        self.trigRate = {0: -1, 3: -1}
        self.getTrigRate(0)
        self.getTrigRate(3)
        self.impendance = {0: -1, 1: -1, 2: -1, 3: -1, 4: -1, 5: -1,
                           6: -1, 7: -1}
        for ii in range(8):
            self.getImpendance(ii)
        self.getTrigSlope()
        self.getBurstCount()
        self.getBurstPeriod()
        self.delay = {2: -1, 3: -1, 5: -1, 6: -1}
        self.relChannel = {2: -1, 3: -1, 5: -1, 6: -1}
        for ii in [2, 3, 5, 6]:
            self.getDelay(ii)
        self.outputMode = {1: -1, 2: -1, 3: -1, 4: -1, 5: -1,
                           6: -1, 7: -1}
        for ii in range(1, 8):
            self.getOutputMode(ii)
        self.outputAmplitude = {1: -1, 2: -1, 3: -1, 4: -1, 5: -1,
                                6: -1, 7: -1}
        for ii in range(1, 8):
            self.getOutputAmplitude(ii)
        self.outputOffset = {1: -1, 2: -1, 3: -1, 4: -1, 5: -1,
                             6: -1, 7: -1}
        for ii in range(1, 8):
            self.getOutputOffset(ii)
        self.outputPolarity = {1: -1, 2: -1, 3: -1, 5: -1,
                               6: -1}
        for ii in [1, 2, 3, 5, 6]:
            self.getOutputPolarity(ii)
        self.logger.debug("Settings acquired.")

    def setTrigMode(self, mode):
        '''Sets the trigger mode

        Mode set to 0 (int), 1 (ext), 2 (SS) or 3 (Burst)

        :param mode: mode to set
        :type: int
        '''
        if mode not in range(4):
            self.logger.error('Mode must be 0,1,2 or 3.')
            raise ValueError('Mode must be 0,1,2 or 3.')
        self.sendRcv('TM ' + str(mode), False)
        self.trigMode = mode
        self.logger.info("Trigger mode set to {}.".format(mode))

    def setExtTrigLevel(self, trigLevel):
        '''Sets the external trigger level in volts.
        :param trigLevel: current external trigger level
        :type: float
        '''
        self.sendRcv('TL' + format(trigLevel, 'e'), False)
        self.extTrigLevel = trigLevel
        self.logger.info("ExTrigLevel set to {}".format(trigLevel))

    def setTrigRate(self, trigMode, trigRate):
        '''Sets the trigger rate

        When device is in burst trigger mode, burst trigger rate will be set.

        :param trigMode: trigger mode to set rate for
        :type: int
        :param trigRate: trigger rate
        :type: float
        '''
        if trigMode == 0:
            self.sendRcv('TR 0,' + format(trigRate, 'e'), False)
        elif trigMode == 3:
            self.sendRcv('TR 1,' + format(trigRate, 'e'), False)
        else:
            self.logger.error('Wrong trigger mode.')
            raise RuntimeError('Wrong trigger mode.')
        self.trigRate[trigMode] = trigRate
        self.logger.info("TrigRate set to {}".format(trigRate))

    def setImpendance(self, channel, impendance):
        '''Set the termination impendance

        Impendance is set to 50 ohms (impendance=0) or high (impendance=1)

        :param channel: terminal
        :type: int
        :param impendance: switch for impendance
        :type: int
        '''
        if channel not in range(8):
            raise ValueError('Channel must be between 0 and 7.')
        self.sendRcv('TZ ' + str(channel) + ',' + str(impendance),
                     False)
        self.impendance[channel] = impendance
        self.logger.info(
            "Impendance of channel {} set to {}".format(channel, impendance))

    def setTrigSlope(self, slope):
        '''Set the trigger slope

        Slope is set to falling (slope=0) or rising(slope=1) edge.

        :param slope: slope selection
        :type: int
        '''
        self.sendRcv('TS ' + str(slope), False)
        self.logger.info("Slope set to {}".format(slope))

    def setBurstCount(self, burstCount):
        '''Set the burst count

        Burst count of pulses per burst. This command is used to specify the
        number of pulses, which will be in each burst of pulses when in
        burst mode.

        :param burstCount: bourst count of pulses per burst
        :type: int
        '''
        self.sendRcv('BC ' + str(burstCount), False)
        self.burstCount = burstCount
        self.logger.info("BurstCount set to {}".format(burstCount))

    def setBurstPeriod(self, burstPeriod):
        '''Set the burst period

        This command specifies the number of triggers between the start of
        each burst of pulses when in burst mode.

        :param burst: bourst period of triggers per burst
        :type: int
        '''
        self.sendRcv('BP ' + str(burstPeriod), False)
        self.burstPeriod = burstPeriod
        self.logger.info("Set burstPeriod to {}".format(burstPeriod))

    def setDelay(self, channel, relChannel, delay):
        '''Set the delay time in seconds

        The function sets the delay of channel, relative to relChannel.
        Can only address the direct channels A(2), B(3), C(5) and D(6).

        :param channel: channel to set
        :type: int
        :param relChannel: channel the delay is set relative to
        :type: int
        :param delay: delay in seconds
        :type: str or Decimal
        '''
        if channel not in [2, 3, 5, 6]:
            raise ValueError('Channel must be 2, 3, 5 or 6')
        if float is type(delay):
            raise TypeError('Delay must be of type str or Decimal')
        if Decimal is not type(delay):
            self.logger.warning("Type of delay was {}, changed to Decimal.".format(type(delay)))
            delay = Decimal(delay)
        self.sendRcv('DT ' + str(channel) + ',' +
                     str(relChannel) + ',' +
                     format(delay, 'e'), False)
        self.relChannel[channel] = relChannel
        self.delay[channel] = delay
        self.logger.info("Set delay for channel {} to {}".format(channel, delay))

    def setOutputMode(self, channel, mode):
        '''Set the output mode for channel to mode

        THe output mode of channel is defined as TTL(0), NIM(1),
        ECL(2) or VARiable(3).

        :param channel: channel to set
        :type: int
        :param mode: output mode
        :type: int
        '''
        if channel not in range(1, 8):
            raise ValueError('Channel must be between 1 and 7')
        self.sendRcv('OM ' + str(channel) + ',' +
                     str(mode), False)
        self.outputMode[channel] = mode
        self.logger.info("Set output mode for channel {} to {}".format(channel, mode))

    def setOutputAmplitude(self, channel, amplitude):
        '''Set the output amplitude for channel in VAR mode

        :param channel: channel to set
        :type: int
        :param amplitude: output amplitude in volts
        :type: float
        '''
        if channel not in range(1, 8):
            raise ValueError('Channel must be between 1 and 7')
        self.sendRcv('OA ' + str(channel) + ',' +
                     format(amplitude, 'e'), False)
        self.outputAmplitude[channel] = amplitude
        self.logger.info("Set output amplitude for channel {} to {}".format(channel, amplitude))

    def setOutputOffset(self, channel, offset):
        '''Set the output amplitude for channel in VAR mode

        :param channel: channel to set
        :type: int
        :param offset: output offset in volts
        :type: float
        '''
        if channel not in range(1, 8):
            raise ValueError('Channel must be between 1 and 7')
        self.sendRcv('OO ' + str(channel) + ',' + format(offset, 'e'), False)
        self.outputOffset[channel] = offset
        self.logger.info("Set offset for channel {} to {}".format(channel, offset))

    def setOutputPolarity(self, channel, polarity):
        '''Set the output polarity for channel

        Ouput Polarity of channel is inverted (0) or normal (1).

        :param channel: channel to set
        :type: int
        :param polarity: output polarity
        :type: int
        '''
        if channel not in [1, 2, 3, 5, 6]:
            raise ValueError('Channel must be 1,2,3,5 or 6.')
        self.sendRcv('OP ' + str(channel) + ',' + str(polarity), False)
        self.outputPolarity[channel] = polarity
        self.logger.info("Set polarity for channel {} to {}".format(channel, polarity))

    def getTrigMode(self):
        '''Gets the trigger mode
        :returns mode: current trigger mode
        :rtype: int
        '''
        self.trigMode = int(self.sendRcv('TM'))
        return self.trigMode

    def getExtTrigLevel(self):
        '''Gets the external trigger level in volts.
        :returns trigLevel: current external trigger level
        :rtype: float
        '''
        self.extTrigLevel = float(self.sendRcv('TL'))
        return self.extTrigLevel

    def getTrigRate(self, trigMode):
        '''Gets the trigger rate

        When device is in burst trigger mode, burst trigger rate will be get.

        :param trigMode: trigger mode to set rate for
        :type: int
        :returns trigRate: trigger rate
        :rtype: float
        '''
        if trigMode == 0:
            answer = self.sendRcv('TR 0')
        elif trigMode == 3:
            answer = self.sendRcv('TR 1')
        else:
            raise RuntimeError('Stanford in wrong trigger mode.')
        self.trigRate[trigMode] = float(answer)
        return self.trigRate[trigMode]

    def getImpendance(self, channel):
        '''Get the termination impendance of selected channel

        Impendance is set to 50 ohms (impendance=0) or high (impendance=1)

        :param channel: terminal to address
        :type: int
        :returns impendance: switch for impendance
        :rtype: int
        '''
        self.impendance[channel] = int(self.sendRcv('TZ ' + str(channel)))
        return self.impendance[channel]

    def getTrigSlope(self):
        '''Set the trigger slope

        Slope is set to falling (slope=0) or rising(slope=1) edge.

        :returns slope: slope selection
        :rtype: int
        '''
        self.slope = int(self.sendRcv('TS'))
        return self.slope

    def getBurstCount(self):
        '''Get the burst count

        Burst count is the number of pulses, which will be in each
        burst of pulses when in burst mode

        :returns burstCount: number of pulses per burst
        :rtype: int
        '''
        self.burstCount = int(self.sendRcv('BC'))
        return self.burstCount

    def getBurstPeriod(self):
        '''Get the burst count

        Burst count is the number of pulses, which will be in each
        burst of pulses when in burst mode

        :returns burstPeriod: burst period of triggers per burst
        :rtype: int
        '''
        self.burstPeriod = int(self.sendRcv('BP'))
        return self.burstPeriod

    def getDelay(self, channel):
        '''Get delay time of channel in seconds

        Can only address the direct channels A(2), B(3), C(5) and D(6).

        :returns relChannel: relative channel for delay
        :rtype: int
        :returns delay: delay of channel in seconds
        :rtype: int
        '''
        if channel not in [2, 3, 5, 6]:
            raise ValueError('Channel must be 2, 3, 5 or 6')
        answer = self.sendRcv('DT ' + str(channel))
        self.relChannel[channel] = int(answer.split(',')[0])
        self.delay[channel] = Decimal(answer.split(',')[1])
        return self.relChannel[channel], self.delay[channel]

    def getOutputMode(self, channel):
        '''Get the output mode for channel

        The output mode of channel is defined as TTL(0), NIM(1),
        ECL(2) or VARiable(3).

        :param channel: channel to set
        :type: int
        :returns mode: output mode
        :rtype: int
        '''
        if channel not in range(1, 8):
            raise ValueError('Channel must be between 1 and 7')
        self.outputMode[channel] = int(self.sendRcv('OM ' + str(channel)))
        return self.outputMode[channel]

    def getOutputAmplitude(self, channel):
        '''Get the output amplitude for channel in VAR mode

        :param channel: channel to set
        :type: int
        :returns amplitude: output amplitude in volts
        :rtype: float
        '''
        if channel not in range(1, 8):
            raise ValueError('Channel must be between 1 and 7')
        self.outputAmplitude[channel] = float(self.sendRcv('OA ' +
                                                           str(channel)))
        return self.outputAmplitude[channel]

    def getOutputOffset(self, channel):
        '''Get the output offset for channel in VAR mode

        :param channel: channel to set
        :type: int
        :returns offset: output offset in volts
        :rtype: float
        '''
        if channel not in range(1, 8):
            raise ValueError('Channel must be between 1 and 7')
        self.outputOffset[channel] = float(self.sendRcv('OO ' +
                                                        str(channel)))
        return self.outputOffset[channel]

    def getOutputPolarity(self, channel):
        '''Get the output polarity for channel

        Ouput Polarity of channel is inverted (0) or normal (1).

        :param channel: channel to set
        :type: int
        :returns polarity: output polarity
        :rtype: int
        '''
        if channel not in [1, 2, 3, 5, 6]:
            raise ValueError('Channel must be 1,2,3,5 or 6.')
        self.outputPolarity[channel] = int(self.sendRcv('OP ' +
                                                        str(channel)))
        return self.outputPolarity[channel]

    def singleShot(self):
        '''Init a delay cycle if in single shot mode.
        '''
        if 2 != self.trigMode:
            raise RuntimeError('Must be in single shot mode.')
        self.sendRcv('SS', False)

    def isBusy(self):
        '''Check for active timing cycle

        :returns busy: status of active timing
        :rtype: bool
        '''
        busy = int(self.sendRcv('IS 1'))
        return bool(busy)

    def clear(self):
        '''Clear instrument

        The communications buffers are cleared and the default settings are
        recalled.
        Afterwards, the settings are called from device.
        '''
        self.sendRcv('CL', False)
        self._getSettings()


if __name__ == '__main__':
    logging.basicConfig(filename="DEBUG_stanfordDG535.log",
                        level=logging.DEBUG,
                        format='%(asctime)s - %(name)s - %(levelname)s'
                               ' - %(message)s')
    with stanfordDG535Control('COM12') as trigBox:
        trigBox.setTrigMode(0)
        trigBox.setOutputPolarity(1, 1)
        print(trigBox.isBusy())
        pass
