stanfordDG535
============
This is an API for the Stanford Research DG535 Four Channel Digital Pulse Generator. The generator is connected to a Prologix GPIB-USB Controller 6.0 for communication, and the serial protocoll is used.

Author: Markus J Schmidt

Version: 0.1.1

class stanfordDG535Error
-----------------------
Exception class is raised

class stanfordDG535Control
------------------
Class object for the pulse generator.

Example
-------

::

	import stanfordDG535 as sf

	with sf.stanfordDG535Control('COM12') as trigBox:
		trigBox.setTrigMode(0)
		trigBox.setTrigRate(0, 20)
		print('Status of trigBox:'. trigBox.isBusy())
