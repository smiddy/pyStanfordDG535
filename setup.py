from setuptools import setup

# version
__version__ = '0.1.1'

setup(
    name="stanfordDG535",
    description="API for Stanford Research DG535 Pulse Generator",
    version=__version__,
    platforms=["any"],
    author="Markus J Schmidt",
    author_email='schmidt@ifd.mavt.ethz.ch',
    license="GNU GPLv3+",
    url="https://github.com/smiddy/berkeley500A",
    py_modules=["stanfordDG535"],
    install_requires=['pyserial>=2.7'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 '
        'or later (GPLv3+)',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    zip_safe=False
)
